# Configure paths for PCRE
# Manish Singh    98-9-30
# stolen back from Frank Belew
# stolen from Manish Singh
# Shamelessly stolen from Owen Taylor

dnl AM_PATH_PCRE([MINIMUM-VERSION, [ACTION-IF-FOUND [, ACTION-IF-NOT-FOUND]]])
dnl Test for PCRE, and define PCRE_CFLAGS and PCRE_LIBS
dnl
AC_DEFUN(AM_PATH_PCRE,
[dnl 
dnl Get the cflags and libraries from the pcre-config script
dnl
AC_ARG_WITH(pcre-prefix,[  --with-pcre-prefix=PFX   Prefix where PCRE is installed (optional)],
            pcre_prefix="$withval", pcre_prefix="")
AC_ARG_WITH(pcre-exec-prefix,[  --with-pcre-exec-prefix=PFX Exec prefix where PCRE is installed (optional)],
            pcre_exec_prefix="$withval", pcre_exec_prefix="")
AC_ARG_ENABLE(pcretest, [  --disable-pcretest       Do not try to compile and run a test PCRE program],
		    , enable_pcretest=yes)

  if test x$pcre_exec_prefix != x ; then
     pcre_args="$pcre_args --exec-prefix=$pcre_exec_prefix"
     if test x${PCRE_CONFIG+set} != xset ; then
        PCRE_CONFIG=$pcre_exec_prefix/bin/pcre-config
     fi
  fi
  if test x$pcre_prefix != x ; then
     pcre_args="$pcre_args --prefix=$pcre_prefix"
     if test x${PCRE_CONFIG+set} != xset ; then
        PCRE_CONFIG=$pcre_prefix/bin/pcre-config
     fi
  fi

  AC_PATH_PROG(PCRE_CONFIG, pcre-config, no)
  min_pcre_version=ifelse([$1], ,0.2.7,$1)
  AC_MSG_CHECKING(for PCRE - version >= $min_pcre_version)
  no_pcre=""
  if test "$PCRE_CONFIG" = "no" ; then
    no_pcre=yes
  else
    PCRE_CFLAGS=`$PCRE_CONFIG $pcreconf_args --cflags`
    PCRE_LIBS=`$PCRE_CONFIG $pcreconf_args --libs`

    pcre_major_version=`$PCRE_CONFIG $pcre_args --version | \
           sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\1/'`
    pcre_minor_version=`$PCRE_CONFIG $pcre_args --version | \
           sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\2/'`
    pcre_micro_version=`$PCRE_CONFIG $pcre_config_args --version | \
           sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`
    if test "x$enable_pcretest" = "xyes" ; then
      ac_save_CFLAGS="$CFLAGS"
      ac_save_LIBS="$LIBS"
      CFLAGS="$CFLAGS $PCRE_CFLAGS"
      LIBS="$LIBS $PCRE_LIBS"
dnl
dnl Now check if the installed PCRE is sufficiently new. (Also sanity
dnl checks the results of pcre-config to some extent
dnl
      rm -f conf.pcretest
      AC_TRY_RUN([
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pcre.h>

char*
my_strdup (char *str)
{
  char *new_str;
  
  if (str)
    {
      new_str = malloc ((strlen (str) + 1) * sizeof(char));
      strcpy (new_str, str);
    }
  else
    new_str = NULL;
  
  return new_str;
}

int main ()
{
  int major, minor, micro;
  char *tmp_version;

  system ("touch conf.pcretest");

  /* HP/UX 9 (%@#!) writes to sscanf strings */
  tmp_version = my_strdup("$min_pcre_version");
  if (sscanf(tmp_version, "%d.%d.%d", &major, &minor, &micro) != 3) {
     printf("%s, bad version string\n", "$min_pcre_version");
     exit(1);
   }

   if (($pcre_major_version > major) ||
      (($pcre_major_version == major) && ($pcre_minor_version > minor)) ||
      (($pcre_major_version == major) && ($pcre_minor_version == minor) && ($pcre_micro_version >= micro)))
    {
      return 0;
    }
  else
    {
      printf("\n*** 'pcre-config --version' returned %d.%d.%d, but the minimum version\n", $pcre_major_version, $pcre_minor_version, $pcre_micro_version);
      printf("*** of PCRE required is %d.%d.%d. If pcre-config is correct, then it is\n", major, minor, micro);
      printf("*** best to upgrade to the required version.\n");
      printf("*** If pcre-config was wrong, set the environment variable PCRE_CONFIG\n");
      printf("*** to point to the correct copy of pcre-config, and remove the file\n");
      printf("*** config.cache before re-running configure\n");
      return 1;
    }
}

],, no_pcre=yes,[echo $ac_n "cross compiling; assumed OK... $ac_c"])
       CFLAGS="$ac_save_CFLAGS"
       LIBS="$ac_save_LIBS"
     fi
  fi
  if test "x$no_pcre" = x ; then
     AC_MSG_RESULT(yes)
     ifelse([$2], , :, [$2])     
  else
     AC_MSG_RESULT(no)
     if test "$PCRE_CONFIG" = "no" ; then
       echo "*** The pcre-config script installed by PCRE could not be found"
       echo "*** If PCRE was installed in PREFIX, make sure PREFIX/bin is in"
       echo "*** your path, or set the PCRE_CONFIG environment variable to the"
       echo "*** full path to pcre-config."
     else
       if test -f conf.pcretest ; then
        :
       else
          echo "*** Could not run PCRE test program, checking why..."
          CFLAGS="$CFLAGS $PCRE_CFLAGS"
          LIBS="$LIBS $PCRE_LIBS"
          AC_TRY_LINK([
#include <stdio.h>
#include <pcre.h>
],      [ return 0; ],
        [ echo "*** The test program compiled, but did not run. This usually means"
          echo "*** that the run-time linker is not finding PCRE or finding the wrong"
          echo "*** version of PCRE. If it is not finding PCRE, you'll need to set your"
          echo "*** LD_LIBRARY_PATH environment variable, or edit /etc/ld.so.conf to point"
          echo "*** to the installed location  Also, make sure you have run ldconfig if that"
          echo "*** is required on your system"
	  echo "***"
          echo "*** If you have an old version installed, it is best to remove it, although"
          echo "*** you may also be able to get things to work by modifying LD_LIBRARY_PATH"],
        [ echo "*** The test program failed to compile or link. See the file config.log for the"
          echo "*** exact error that occured. This usually means PCRE was incorrectly installed"
          echo "*** or that you have moved PCRE since it was installed. In the latter case, you"
          echo "*** may want to edit the pcre-config script: $PCRE_CONFIG" ])
          CFLAGS="$ac_save_CFLAGS"
          LIBS="$ac_save_LIBS"
       fi
     fi
     PCRE_CFLAGS=""
     PCRE_LIBS=""
     ifelse([$3], , :, [$3])
  fi
  AC_SUBST(PCRE_CFLAGS)
  AC_SUBST(PCRE_LIBS)
  rm -f conf.pcretest
])
